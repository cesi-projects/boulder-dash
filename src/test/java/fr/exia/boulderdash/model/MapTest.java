package fr.exia.boulderdash.model;

import fr.exia.boulderdash.model.element.block.Dirt;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MapTest
{

    private static Map map;
    private static int width, height;

    @Before
    public void setUp()
    {
        width = 4;
        height = 4;

        map = new Map(width, height, "DDD  |6DD");
        map.setElement(new Dirt(), 0, 0);
        map.setElement(new Dirt(), 0, 1);
    }

    @Test
    public void Map()
    {
        assertNotNull("Should instantiate a map", map);
    }

    @Test
    public void getElement()
    {
        assertNotNull("Should return an entity.", map.getElement(0, 0));
        assertTrue("Should return the right entity.", map.getElement(0, 1) instanceof Dirt);
    }

    @Test
    public void getElementOutOfTheMap()
    {
        try {
            map.getElement(999, 999);
            fail("Should return an exception ArrayIndexOutOfBoundsException");
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    @Test
    public void getWidth()
    {
        assertEquals("Should return the right value of the map's width.", width, map.getWidth());
    }

    @Test
    public void getHeight()
    {
        assertEquals("Should return the right value of the map's Height.", height, map.getHeight());
    }
}