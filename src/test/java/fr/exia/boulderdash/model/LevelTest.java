package fr.exia.boulderdash.model;

import fr.exia.boulderdash.contract.Direction;
import fr.exia.boulderdash.controller.BoulderDashController;
import fr.exia.boulderdash.model.dao.LevelDAO;
import fr.exia.boulderdash.model.db.DBConnection;
import fr.exia.boulderdash.model.db.DBPopulate;
import fr.exia.boulderdash.view.BoulderDashView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.awt.*;

import static org.junit.Assert.*;

public class LevelTest
{

    private User user;
    private Level level;
    private LevelDAO levelDAO = null;
    private BoulderDashController boulderDashController;
    private BoulderDashView view;
    private BoulderDashModel model;
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp()
    {
        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.createLevel();
        levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());

        model = new BoulderDashModel();
        view = new BoulderDashView(model);

        boulderDashController = new BoulderDashController(view, model);
        view.setController(boulderDashController);

        level = Level.getFirst();
        user = new User("test_user");
        user.setLevel(level);
        model.setUser(user);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void getFirst()
    {
        assertNotNull(levelDAO.findByLvlNumber(1));
    }

  /*  @Test
    public void getLevelNotExist()
    {
        try {
            Level byLvlNumber = levelDAO.findByLvlNumber(59595);
            System.out.println(byLvlNumber);
            fail("Should throw exception when level not found");
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            assertEquals("level not found", e.getMessage());
        }
    }*/

    @Test
    public void getAll()
    {
        for (int i = 1; i < 5; i++) {
            assertNotNull(levelDAO.findByLvlNumber(i));
        }
    }

    @Test
    public void getLvlNumber()
    {
    }

    @Test
    public void getMap()
    {
        assertNotNull(Level.getFirst().getMap());
    }

    @Test
    public void isPaused()
    {
        new Thread(level::start).start();

        final Point pos_1 = (Point) level.getRockford().getPosition().clone();

        level.setPaused(true);
        boulderDashController.orderPerform(Direction.RIGHT);
        boulderDashController.orderPerform(Direction.DOWN);

        final Point pos_2 = (Point) level.getRockford().getPosition().clone();

        assertEquals(pos_1, pos_2);
    }
}