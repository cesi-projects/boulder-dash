#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

--
-- Base de données :  `boulderdash`
--
CREATE DATABASE IF NOT EXISTS `boulderdash`;

USE `boulderdash`;

DELIMITER $$

DROP PROCEDURE IF EXISTS getUserByUsername$$
CREATE PROCEDURE getUserByUsername(IN `p_username` VARCHAR(50))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    SELECT *
    FROM user
    WHERE user.username = p_username;
END$$

DROP PROCEDURE IF EXISTS createUser$$
CREATE PROCEDURE `createUser`(IN `p_username` VARCHAR(100), `p_id_level` INT(11))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    INSERT INTO user(`username`, `id_level`)
    VALUES (p_username, p_id_level);
END$$

DROP PROCEDURE IF EXISTS updateUser$$
CREATE PROCEDURE `updateUser`(IN `p_id` INT(11), `p_username` VARCHAR(100), `p_id_level` INT(11))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    UPDATE user
    SET user.username = p_username,
        user.id_level = p_id_level
    WHERE user.id = p_id;
END$$

DROP PROCEDURE IF EXISTS getLevelById$$
CREATE PROCEDURE `getLevelById`(IN `p_id` INT(100))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    SELECT *
    FROM level
    WHERE level.id = p_id;
END$$

DROP PROCEDURE IF EXISTS getLevelByNumber$$
CREATE PROCEDURE `getLevelByNumber`(IN `p_lvl_number` INT(100))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    SELECT *
    FROM level
    WHERE level.lvl_number = p_lvl_number;
END$$

DROP PROCEDURE IF EXISTS createLevel$$
CREATE PROCEDURE `createLevel`(IN `p_map` VARCHAR(10000), `p_map_height` INT(11), `p_map_width` INT(11),
                               `p_name` VARCHAR(100), `p_lvl_number` INT(11))
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    INSERT INTO level(`map`, `map_height`, `map_width`, `name`, `lvl_number`)
    VALUES (p_map, p_map_height, p_map_width, p_name, p_lvl_number);
END$$

DROP PROCEDURE IF EXISTS getFirstLevel$$
CREATE PROCEDURE `getFirstLevel`()
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    SELECT *
    FROM level
    WHERE level.lvl_number = (SELECT MIN(lvl_number) FROM level);
END$$

DROP PROCEDURE IF EXISTS getAllLevels$$
CREATE PROCEDURE `getAllLevels`()
    READS SQL DATA SQL SECURITY INVOKER
BEGIN
    SELECT *
    FROM level;
END$$

DELIMITER ;

#------------------------------------------------------------
# Table: level
#------------------------------------------------------------

CREATE TABLE IF NOT EXISTS level
(
    id         Int Auto_increment NOT NULL,
    map        Varchar(10000)     NOT NULL,
    map_height Int(11)            NOT NULL,
    map_width  Int(11)            NOT NULL,
    name       Varchar(100)       NOT NULL,
    lvl_number Int                NOT NULL UNIQUE,
    INDEX Level_Idx (lvl_number),
    CONSTRAINT Level_PK PRIMARY KEY (id)
) ENGINE = InnoDB;
ALTER TABLE `level`
    AUTO_INCREMENT = 1;

#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE IF NOT EXISTS user
(
    id       Int Auto_increment NOT NULL,
    username Varchar(100)       NOT NULL,
    id_level Int                NOT NULL,
    INDEX User_Idx (username),
    CONSTRAINT User_PK PRIMARY KEY (id),
    CONSTRAINT User_Level_FK FOREIGN KEY (id_level) REFERENCES Level (id)
) ENGINE = InnoDB;
