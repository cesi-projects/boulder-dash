package fr.exia.boulderdash;

import fr.exia.boulderdash.controller.BoulderDashController;
import fr.exia.boulderdash.model.BoulderDashModel;
import fr.exia.boulderdash.model.db.DBPopulate;
import fr.exia.boulderdash.view.BoulderDashView;


public class Main {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(final String[] args) {

        DBPopulate dbPopulate = new DBPopulate();
        dbPopulate.createLevel();

        final BoulderDashModel model = new BoulderDashModel();
        final BoulderDashView view = new BoulderDashView(model);

        final BoulderDashController boulderDashController = new BoulderDashController(view, model);
        view.setController(boulderDashController);
    }
}
