package fr.exia.boulderdash.controller;

import fr.exia.boulderdash.contract.Direction;
import fr.exia.boulderdash.contract.IController;
import fr.exia.boulderdash.contract.IModel;
import fr.exia.boulderdash.contract.IView;
import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.block.Door;
import fr.exia.boulderdash.model.element.block.Empty;
import fr.exia.boulderdash.model.element.mobile.Diamond;
import fr.exia.boulderdash.model.element.mobile.Mob;

import java.awt.*;

/**
 * The Class Controller.
 */
public final class BoulderDashController implements IController {

    /**
     * The view.
     */
    private IView view;

    /**
     * The model.
     */
    private IModel model;

//    private Level level;
//    private User user;

    /**
     * Instantiates a new controller.
     *
     * @param view  the view
     * @param model the model
     */
    public BoulderDashController(final IView view, final IModel model) {
        this.setView(view);
        this.setModel(model);
    }

    /**
     * Sets the view.
     *
     * @param pview the new view
     */
    private void setView(final IView pview) {
        this.view = pview;
    }

    /**
     * Sets the model.
     *
     * @param model the new model
     */
    private void setModel(final IModel model) {
        this.model = model;
    }

    /**
     * Order perform.
     *
     * @param direction the controller order
     */
    public void orderPerform(Direction direction) {
        Level level = this.model.getUser().getLevel();
        if (level.isPaused() || level.isFinished())
            return;
        Point point = (Point) level.getRockford().getPosition().clone();
        switch (direction) {
            case UP:
                point.y--;
                move(level, point, direction);
                break;
            case DOWN:
                point.y++;
                move(level, point, direction);
                break;
            case LEFT:
                point.x--;
                move(level, point, direction);
                break;
            case RIGHT:
                point.x++;
                move(level, point, direction);
                break;
            default:
                break;
        }
    }

    /**
     * Pause the game
     */
    @Override
    public void togglePause() {
        Level level = model.getUser().getLevel();
        level.setPaused(!level.isPaused());
    }

    /**
     * Start the game
     */
    @Override
    public void startGame() {
        model.getUser().getLevel().start();
    }

    /**
     * Diamond collecting, moving of the player into dirt or empty block
     *
     * @param level     The level of the user
     * @param point     The position of the user
     * @param direction The movement of the user
     */
    private void move(Level level, Point point, Direction direction) {
        Element element = level.getMap().get(point);
        if (element.isBreakable())
            level.getMap().setElement(new Empty(), point);
        if (element.isBlocking())
            return;
        if (element instanceof Diamond)
            level.collectDiamond((Diamond) element);
        this.model.getRockford().move(direction);
        if (element instanceof Door) {
            level.finish();
        }
        if (element instanceof Mob) {
            level.die();
        }
    }
}
