package fr.exia.boulderdash.model.element.mobile;

import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Rockford
 */
public class Rockford extends Mobile
{
    /**
     * The sprite Rockford is associated with the character P and the image name mobile/rockford
     */
    private static final Sprite SPRITE = new Sprite('P', "mobile/rockford.png");

    public Rockford(){
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return Rockford can be move.
     */
    @Override
    public boolean isMovable()
    {
        return true;
    }

    /**
     * Animate an element
     *
     * @return Rockford is animated.
     */
    @Override
    public boolean isAnimated()
    {
        return true;
    }

    /**
     * Break an element
     *
     * @return Rockford can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return Rockford can block.
     */
    @Override
    public boolean isBlocking()
    {
        return true;
    }

    /**
     * Gravity of an element
     *
     * @return Rockford doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
