package fr.exia.boulderdash.model.element.mobile;

import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Mob
 */
public class Mob extends Mobile
{
    /**
     * The sprite Mob is associated with the character M and the image name mobile/octopus
     */
    private static final Sprite SPRITE = new Sprite('M', "mobile/octopus.png");

    public Mob()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The mob can't be move.
     */
    @Override
    public boolean isMovable()
    {
        return false;
    }

    /**
     * Animate an element
     *
     * @return The mob is not animated.
     */
    @Override
    public boolean isAnimated()
    {
        return false;
    }

    /**
     * Break an element
     *
     * @return The mob can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return The mob can't block.
     */
    @Override
    public boolean isBlocking()
    {
        return false;
    }

    /**
     * Gravity of an element
     *
     * @return The mob doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
