package fr.exia.boulderdash.model.element.block;

import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Empty
 */
public class Empty extends Element
{
    /**
     * The sprite Empty is associated with the character " " and the image name empty
     */
    private static final Sprite SPRITE = new Sprite(' ', "empty.png");

    public Empty()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return Empty can't be move.
     */
    @Override
    public boolean isMovable()
    {
        return false;
    }

    /**
     * Animate an element
     *
     * @return Empty can't be animated.
     */
    @Override
    public boolean isAnimated()
    {
        return false;
    }

    /**
     * Break an element
     *
     * @return Empty can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return Empty can't block.
     */
    @Override
    public boolean isBlocking()
    {
        return false;
    }

    /**
     * Gravity of an element
     *
     * @return Empty doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
