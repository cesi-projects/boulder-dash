package fr.exia.boulderdash.model.element;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * The class Sprite
 */
public class Sprite
{
    private Image image;
    private String imageName;
    private char character;

    /**
     * Instantiates a new sprite.
     *
     * @param character the character
     * @param imageName the image name
     */
    public Sprite(final char character, final String imageName)
    {
        this.character = character;
        this.imageName = imageName;
        try {
            loadImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Instantiates a new sprite.
     *
     * @param character the character
     */
    public Sprite(final char character)
    {
        this(character, "/img/element/empty.jpg");
    }

    /**
     * Get the image.
     *
     * @return the image
     */
    public final Image getImage()
    {
        return this.image;
    }

    /**
     * Load image.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void loadImage() throws IOException
    {
        InputStream resourceAsStream = Sprite.class.getResourceAsStream("/img/element/" + this.getImageName());
        assert resourceAsStream != null;
        this.image = ImageIO.read(resourceAsStream);
    }

    /**
     * Get the console image.
     *
     * @return the consoleImage
     */
    public final char getCharacter()
    {
        return this.character;
    }

    /**
     * Set the image.
     *
     * @param image the new image
     */
    private void setImage(final Image image)
    {
        this.image = image;
    }

    /**
     * Set the character.
     *
     * @param character the character to set
     */
    private void setCharacter(final char character)
    {
        this.character = character;
    }

    /**
     * Get the image name.
     *
     * @return the imageName
     */
    public final String getImageName()
    {
        return this.imageName;
    }

    /**
     * Set the image name.
     *
     * @param imageName the imageName to set
     */
    private void setImageName(final String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * For an element to be embedded in a map
     *
     * @return A sprite associated with an image, a name and a character
     */
    @Override
    public String toString()
    {
        return "Sprite{" +
                "image=" + image +
                ", imageName='" + imageName + '\'' +
                ", character=" + character +
                '}';
    }
}
