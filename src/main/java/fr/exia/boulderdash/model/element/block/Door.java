package fr.exia.boulderdash.model.element.block;

import fr.exia.boulderdash.model.element.Sprite;
import fr.exia.boulderdash.model.element.mobile.Mobile;


/**
 * The class Door
 */
public class Door extends Mobile
{
    /**
     * The sprite Door is associated with the character L and the image name door
     */
    private static final Sprite SPRITE = new Sprite('L', "door.png");

    public Door()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The door can't be move.
     */
    @Override
    public boolean isMovable()
    {
        return false;
    }

    /**
     * Animate an element
     *
     * @return The door is animated.
     */
    @Override
    public boolean isAnimated()
    {
        return true;
    }

    /**
     * Break an element
     *
     * @return The door can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return The door can't block.
     */
    @Override
    public boolean isBlocking()
    {
        return false;
    }

    /**
     * Gravity of an element
     *
     * @return The door doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
