package fr.exia.boulderdash.model.element.mobile;

import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Diamond
 */
public class Diamond extends Mobile
{
    /**
     * The sprite Diamond is associated with the character 6 and the image name diamond
     */
    private static final Sprite SPRITE = new Sprite('6', "mobile/diamond.png");

    public Diamond()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The diamond can be move.
     */
    @Override
    public boolean isMovable()
    {
        return true;
    }

    /**
     * Animate an element
     *
     * @return The diamond is not animated.
     */
    @Override
    public boolean isAnimated()
    {
        return false;
    }

    /**
     * Break an element
     *
     * @return The diamond is breakable.
     */
    @Override
    public boolean isBreakable()
    {
        return true;
    }

    /**
     * Block an element
     *
     * @return The diamond can't block.
     */
    @Override
    public boolean isBlocking()
    {
        return false;
    }

    /**
     * Gravity of an element
     *
     * @return The diamond has gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return true;
    }
}
