package fr.exia.boulderdash.model.element.mobile;

import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Rock
 */
public class Rock extends Mobile
{
    /**
     * The sprite Rock is associated with the character 0 and the image name mobile/rock
     */
    private static final Sprite SPRITE = new Sprite('0', "mobile/rock.png");

    public Rock()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The rock can be move.
     */
    @Override
    public boolean isMovable()
    {
        return true;
    }

    /**
     * Animate an element
     *
     * @return The rock is animated.
     */
    @Override
    public boolean isAnimated()
    {
        return true;
    }

    /**
     * Break an element
     *
     * @return The rock can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return The rock can block.
     */
    @Override
    public boolean isBlocking()
    {
        return true;
    }

    /**
     * Gravity of an element
     *
     * @return The rock has gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return true;
    }
}
