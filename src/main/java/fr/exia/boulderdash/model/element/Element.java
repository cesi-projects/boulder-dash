package fr.exia.boulderdash.model.element;

import fr.exia.boulderdash.contract.IElement;
import fr.exia.boulderdash.model.element.block.Empty;

import java.awt.*;

/**
 * The class Element
 */
public abstract class Element implements IElement, Cloneable
{
    private Sprite sprite;

    /**
     * Element
     *
     * @param sprite an element have a sprite
     */
    public Element(final Sprite sprite)
    {
        this.sprite = sprite;
    }

    /**
     * Gets the sprite.
     *
     * @return the sprite
     */
    public Sprite getSprite()
    {
        return this.sprite;
    }

    /**
     * Sets the sprite.
     *
     * @param sprite the new sprite
     */
    public void setSprite(final Sprite sprite)
    {
        this.sprite = sprite;
    }

    /**
     * Image
     *
     * @return a sprite associated with an image
     */
    @Override
    public final Image getImage()
    {
        return this.sprite.getImage();
    }

    /**
     * Associate an element to a sprite
     *
     * @return The sprite of an element
     */
    @Override
    public String toString()
    {
        return "Element{" +
                "sprite=" + sprite +
                '}';
    }

    /**
     * Avoid clone
     *
     * @return No clone is allowed
     */
    public Element clone() {
        try {
            return (Element) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
