package fr.exia.boulderdash.model.element;


import fr.exia.boulderdash.model.element.block.*;
import fr.exia.boulderdash.model.element.mobile.Diamond;
import fr.exia.boulderdash.model.element.mobile.Mob;
import fr.exia.boulderdash.model.element.mobile.Rock;
import fr.exia.boulderdash.model.element.mobile.Rockford;

/**
 * The class ElementFactory
 */
public class ElementFactory
{

    /**
     * The Constant MOB.
     */
    private static final Mob MOB = new Mob();

    /**
     * The Constant DIRT.
     */
    private static final Dirt DIRT = new Dirt();

    /**
     * The Constant DIAMOND.
     */
    private static final Diamond DIAMOND = new Diamond();

    /**
     * The Constant ROCK.
     */
    private static final Rock ROCK = new Rock();

    /**
     * The Constant DOOR.
     */
    private static final Door DOOR = new Door();

    /**
     * The Constant WALL.
     */
    private static final Wall WALL = new Wall();

    /**
     * The Constant EMPTY.
     */
    private static final Empty EMPTY = new Empty();

    /**
     * The Constant ROCKFORD
     */
    private static final Rockford ROCKFORD = new Rockford();
    /**
     * List of element
     */
    private static Element[] elements = {
            MOB,
            ROCKFORD,
            DIRT,
            DIAMOND,
            ROCK,
            DOOR,
            WALL,
            EMPTY
    };

    /**
     * getFromChar
     *
     * @param character a character associated with an element
     * @return an element associated with a character
     */
    public static Element getFromChar(char character)
    {
        for (Element element : elements) {
            if (element.getSprite().getCharacter() == character) {
                    return element.clone();
            }
        }
        return EMPTY;
    }
}
