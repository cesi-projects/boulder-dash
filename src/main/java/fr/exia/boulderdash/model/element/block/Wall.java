package fr.exia.boulderdash.model.element.block;

import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Wall
 */
public class Wall extends Element
{
    /**
     * The sprite Wall is associated with the character | and the image name wall
     */
    private static final Sprite SPRITE = new Sprite('|', "wall.png");

    public Wall()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The wall can't be move.
     */
    @Override
    public boolean isMovable()
    {
        return false;
    }

    /**
     * Animate an element
     *
     * @return The wall is not animated.
     */
    @Override
    public boolean isAnimated()
    {
        return false;
    }

    /**
     * Break an element
     *
     * @return The wall can't be break.
     */
    @Override
    public boolean isBreakable()
    {
        return false;
    }

    /**
     * Block an element
     *
     * @return The wall can block.
     */
    @Override
    public boolean isBlocking()
    {
        return true;
    }

    /**
     * Gravity of an element
     *
     * @return The wall doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
