package fr.exia.boulderdash.model.element.mobile;

import fr.exia.boulderdash.contract.IPawn;
import fr.exia.boulderdash.contract.Direction;
import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.Sprite;

import java.awt.*;

/**
 * The abstract class Mobile
 */
public abstract class Mobile extends Element implements IPawn {
    private Point position;

    /**
     * Assigns a sprite to a position
     *
     * @param sprite Image of an element
     */
    public Mobile(Sprite sprite) {
        super(sprite);
        this.position = new Point();
    }

    /**
     * Assigns a sprite to a coordinate
     *
     * @param sprite Image of an element
     * @param x Position x
     * @param y Position y
     */
    public Mobile(Sprite sprite, int x, int y) {
        this(sprite);
        this.position.x = x;
        this.position.y = y;
    }

    /**
     * The movement of Rockford
     *
     * @param direction The directions, up, down, right, left
     */
    public void move(Direction direction) {
        switch (direction) {
            case UP:
                position.y--;
                break;
            case DOWN:
                position.y++;
                break;
            case LEFT:
                position.x--;
                break;
            case RIGHT:
                position.x++;
                break;
        }
    }

    /**
     * Get the position x
     *
     * @return Position x
     */
    @Override
    public int getX()
    {
        return position.x;
    }

    /**
     * Get the position y
     *
     * @return Position y
     */
    @Override
    public int getY()
    {
        return position.y;
    }

    /**
     * Get position x, y
     *
     * @return Position
     */
    @Override
    public Point getPosition()
    {
        return position;
    }

    /**
     * Set position
     *
     * @param position Configure position
     */
    @Override
    public void setPosition(Point position)
    {
        this.position = position;
    }
}