package fr.exia.boulderdash.model.element.block;

import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.Sprite;

/**
 * The class Dirt
 */
public class Dirt extends Element
{
    /**
     * The sprite Dirt is associated with the character D and the image name dirt
     */
    private static final Sprite SPRITE = new Sprite('D', "dirt.png");

    public Dirt()
    {
        super(SPRITE);
    }

    /**
     * Push an element
     *
     * @return The dirt can't be move.
     */
    @Override
    public boolean isMovable()
    {
        return false;
    }

    /**
     * Animate an element
     *
     * @return The dirt is not animated.
     */
    @Override
    public boolean isAnimated()
    {
        return false;
    }

    /**
     * Break an element
     *
     * @return The dirt is breakable.
     */
    @Override
    public boolean isBreakable()
    {
        return true;
    }

    /**
     * Block an element
     *
     * @return The dirt can't block.
     */
    @Override
    public boolean isBlocking()
    {
        return false;
    }

    /**
     * Gravity of an element
     *
     * @return The dirt doesn't have gravity.
     */
    @Override
    public boolean hasGravity()
    {
        return false;
    }
}
