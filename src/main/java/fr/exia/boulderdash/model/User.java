package fr.exia.boulderdash.model;

import fr.exia.boulderdash.model.dao.LevelDAO;
import fr.exia.boulderdash.model.dao.UserDAO;
import fr.exia.boulderdash.model.db.DBConnection;

/**
 * The class User
 */
public class User
{
    private int id;
    private String username;
    private Level level;
    private LevelDAO levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());
    private UserDAO userDAO = new UserDAO(DBConnection.getInstance().getConnection());

    public User()
    {
    }

    /**
     * The id, username, level of the user
     *
     * @param username Name of the user
     */
    public User(String username)
    {
        this(0, username, null);
    }

    /**
     * The user
     *
     * @param username Name of the player
     * @param level The level
     * @param id The id of the user
     */
    public User(int id, String username, Level level)
    {
        this.id = id;
        this.username = username;
        this.level = level;
        if (level != null)
            this.level.setUser(this);
    }

    /**
     * Find username of the user
     *
     * @param username Name of the player
     * @return The user
     */
    public static User findByUsername(String username)
    {
        UserDAO userDAO = new UserDAO(DBConnection.getInstance().getConnection());
        User user = userDAO.findByUsername(username);
        if (user == null) {
            user = new User(username);
            user.setLevel(Level.getFirst());
            userDAO.create(user);
        }
        return user;
    }

    /**
     * Next level
     */
    public void levelUp()
    {
        Level nextLevel = levelDAO.findByLvlNumber(getLevel().getLvlNumber() + 1);
        setLevel(nextLevel);
        userDAO.update(this);
    }

    /**
     * Get username
     *
     * @return The username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Set username
     *
     * @param username Name of the player
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * Get level
     *
     * @return The level
     */
    public Level getLevel()
    {
        return level;
    }

    /**
     * Set level
     *
     * @param level Actual level of the user
     */
    public void setLevel(Level level)
    {
        this.level = level;
        this.level.setUser(this);
    }

    /**
     * Get id
     *
     * @return Id of the user
     */
    public int getId()
    {
        return id;
    }
}
