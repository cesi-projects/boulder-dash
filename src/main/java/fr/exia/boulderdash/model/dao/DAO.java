package fr.exia.boulderdash.model.dao;

import java.sql.Connection;

/**
 * The Class DAO.
 *
 * @param <E> the element type
 */
abstract class DAO<E>
{

    /**
     * The connection.
     */
    private final Connection connection;

    /**
     * Instantiates a new DAO.
     *
     * @param connection the connection
     */
    public DAO(final Connection connection)
    {
        this.connection = connection;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    protected Connection getConnection()
    {
        return this.connection;
    }

    /**
     * Creates
     *
     * @param obj the object
     * @return true, if successful
     */
    public abstract boolean create(E obj);

    /**
     * Delete
     *
     * @param obj the object
     * @return true, if successful
     */
    public abstract boolean delete(E obj);

    /**
     * Update
     *
     * @param obj the object
     * @return true, if successful
     */
    public abstract boolean update(E obj);

    /**
     * Find
     *
     * @param id the id
     * @return T
     */
    public abstract E find(int id);

}
