package fr.exia.boulderdash.model.dao;

import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.model.Map;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The class LevelDAO
 */
public class LevelDAO extends DAO<Level>
{
    /**
     * Instantiates a new DAO.
     *
     * @param connection the connection
     */
    public LevelDAO(Connection connection)
    {
        super(connection);
    }

    /**
     * Create a level
     *
     * @param level Create level
     * @return new level
     */
    @Override
    public boolean create(Level level)
    {
        try {
            final String sql = "{call createLevel(?,?,?,?,?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setString(1, level.getMap().getContent());
            call.setInt(2, level.getMap().getHeight());
            call.setInt(3, level.getMap().getWidth());
            call.setString(4, level.getName());
            call.setInt(5, level.getLvlNumber());
            call.execute();
            return true;
        } catch (SQLException e) {
//            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete a level
     *
     * @param obj the object
     * @return delete the level
     */
    @Override
    public boolean delete(Level obj)
    {
        return false;
    }

    /**
     * Update a level
     *
     * @param obj the object
     * @return update the level
     */
    @Override
    public boolean update(Level obj)
    {
        return false;
    }

    /**
     * Find id level
     *
     * @param id The id of the level
     * @return Create level
     */
    @Override
    public Level find(int id)
    {
        try {
            final String sql = "{call getLevelById(?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setInt(1, id);
            return createLevel(call);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * First level
     *
     * @return Create level
     */
    public Level findFirstLevel()
    {
        try {
            final String sql = "{call getFirstLevel()}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            return createLevel(call);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Find level number
     *
     * @param lvlNumber Get level number
     * @return Create level
     */
    public Level findByLvlNumber(int lvlNumber)
    {
        try {
            final String sql = "{call getLevelByNumber(?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setInt(1, lvlNumber);
            return createLevel(call);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create level with the width, height, id, lvl number and name of the map
     *
     * @param call Call stored procedure
     * @return New level
     * @throws SQLException "sql error"
     */
    private Level createLevel(CallableStatement call) throws Exception
    {
        call.execute();
        final ResultSet resultSet = call.getResultSet();
        if (resultSet.first()) {
            Map map = new Map(resultSet.getInt("map_width"),
                              resultSet.getInt("map_height"),
                              resultSet.getString("map"));
            return new Level(resultSet.getInt("id"),
                             resultSet.getInt("lvl_number"),
                             resultSet.getString("name"),
                             map);
        } else {
            throw new Exception("level not found");
        }
    }

    /**
     * Get all Levels
     *
     * @return All levels
     */
    public ArrayList<Level> getAll()
    {
        ArrayList<Level> lvAL = new ArrayList<>();
        try {
            final String sql = "{call getAllLevels()}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.execute();
            final ResultSet resultSet = call.getResultSet();
            while (resultSet.next()) {
                Map map = new Map(resultSet.getInt("map_width"),
                                  resultSet.getInt("map_height"),
                                  resultSet.getString("map"));
                lvAL.add(new Level(resultSet.getInt("id"), resultSet.getInt("lvl_number"), resultSet.getString("name"), map));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lvAL;
    }
}
