package fr.exia.boulderdash.model.dao;

import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.model.User;
import fr.exia.boulderdash.model.db.DBConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class UserDAO
 */
public class UserDAO extends DAO<User>
{

    /**
     * Instantiates a new DAO.
     *
     * @param connection the connection
     */
    public UserDAO(Connection connection)
    {
        super(connection);
    }

    /**
     * Create a new user
     *
     * @param user create user
     * @return a new user
     */
    @Override
    public boolean create(User user)
    {
        try {
            final String sql = "{call createUser(?, ?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setString(1, user.getUsername());
            call.setInt(2, user.getLevel().getId());
            call.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Delete a user
     *
     * @param user delete user
     * @return no more user
     */
    @Override
    public boolean delete(User user)
    {
        return false;
    }

    /**
     * Update a user
     *
     * @param user update user
     * @return edit user
     */
    @Override
    public boolean update(User user)
    {
        try {
            final String sql = "{call updateUser(?, ?, ?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setInt(1, user.getId());
            call.setString(2, user.getUsername());
            call.setInt(3, user.getLevel().getId());
            call.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * User id
     *
     * @param id the id
     * @return find the id of the user
     */
    @Override
    public User find(int id)
    {
        return null;
    }

    /**
     * Find user by username
     *
     * @param username Username of the user
     * @return new user
     */
    public User findByUsername(String username)
    {
        try {
            final String sql = "{call getUserByUsername(?)}";
            final CallableStatement call = this.getConnection().prepareCall(sql);
            call.setString(1, username);
            call.execute();
            final ResultSet resultSet = call.getResultSet();
            if (resultSet.first()) {
                LevelDAO levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());
                Level level = levelDAO.find(resultSet.getInt("id_level"));
                return new User(resultSet.getInt("id"), resultSet.getString("username"), level);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
