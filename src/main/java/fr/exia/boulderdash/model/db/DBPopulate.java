package fr.exia.boulderdash.model.db;

import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.model.Map;

/**
 * Created by Matiyeu on 03/06/2019.
 **/

/**
 * The class DBPopulate
 */
public class DBPopulate
{
    public DBPopulate()
    {
        createLevel();
    }

    /**
     * Generate all maps
     */
    public void createLevel()
    {
        Level level1 = new Level(1, 1, "Cave", new Map("map/map_1.txt"));
        level1.insert();
        Level level2 = new Level(2, 2, "Cave", new Map("map/map_2.txt"));
        level2.insert();
        Level level3 = new Level(3, 3, "Cave", new Map("map/map_3.txt"));
        level3.insert();
        Level level4 = new Level(4, 4, "Cave", new Map("map/map_4.txt"));
        level4.insert();
        Level level5 = new Level(5, 5, "Cave", new Map("map/map_5.txt"));
        level5.insert();
    }
}
