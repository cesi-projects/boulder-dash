package fr.exia.boulderdash.model;

import fr.exia.boulderdash.contract.IModel;
import fr.exia.boulderdash.model.element.mobile.Rockford;

/**
 * The class BoulderDashModel
 */
public final class BoulderDashModel implements IModel
{
    private User user;

    /**
     * Set user
     *
     * @param user The user
     */
    @Override
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * Get user
     *
     * @return The user
     */
    @Override
    public User getUser()
    {
        return user;
    }

    /**
     * Set Rockford
     *
     * @param rockford The player
     */
    @SuppressWarnings("Not used")
    public void setRockford(Rockford rockford)
    {
        this.user.getLevel().getMap().setRockford(rockford);
    }

    /**
     * Get Rockford
     *
     * @return Rockford level
     */
    @Override
    public Rockford getRockford()
    {
        return this.user.getLevel().getRockford();
    }
}
