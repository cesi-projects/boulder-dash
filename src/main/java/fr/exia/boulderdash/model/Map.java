package fr.exia.boulderdash.model;

import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.ElementFactory;
import fr.exia.boulderdash.model.element.block.Dirt;
import fr.exia.boulderdash.model.element.block.Door;
import fr.exia.boulderdash.model.element.block.Empty;
import fr.exia.boulderdash.model.element.block.Wall;
import fr.exia.boulderdash.model.element.mobile.Diamond;
import fr.exia.boulderdash.model.element.mobile.Mobile;
import fr.exia.boulderdash.model.element.mobile.Rockford;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Observable;

/**
 * The class Map
 */
public class Map extends Observable
{
    private int width;
    private int height;
    private String content = "";
    private Element[][] elements;

    private Rockford rockford;
    private Door door;
    private ArrayList<Mobile> elementsMobile = new ArrayList<>();
    ArrayList<Diamond> diamonds = new ArrayList<>();

    /**
     * List of the mobile elements
     *
     * @return Mobile elements
     */
    public ArrayList<Mobile> getElementsMobile()
    {
        return this.elementsMobile;
    }

    public Map()
    {
        this(0, 0, "");
    }

    /**
     * Position of element in the map
     *
     * @param width   x position
     * @param height  y position
     * @param content element
     */
    public Map(int width, int height, String content)
    {
        this.width = width;
        this.height = height;
        this.content = content;
        loadElementsFromContent();
    }

    /**
     * Generates the elements in the map
     *
     * @param fileName Map name
     */
    public Map(String fileName)
    {
        try {
            this.loadFile(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        loadElementsFromContent();
    }

    /**
     * Associate map elements to x, y coordinates
     */
    private void loadElementsFromContent()
    {
        this.elements = new Element[this.width][this.height];
        this.elementsMobile.clear();
        String[] rows = content.split(";");
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                if (rows.length - 1 < y || rows[y].toCharArray().length - 1 < x) {
                    this.setElement(new Wall(), x, y);
                } else {
                    Element element = ElementFactory.getFromChar(rows[y].toCharArray()[x]);
                    if (element instanceof Mobile) {
                        Mobile elementMobile = (Mobile) element;
                        elementMobile.setPosition(new Point(x, y));
                        if (element instanceof Door) {
                            this.setElement(new Dirt(), x, y);
                            this.door = (Door) element;
                            continue;
                        }
                        if (element instanceof Diamond) {
                            this.diamonds.add((Diamond) element);
                        }
                        if (element instanceof Rockford) {
                            this.rockford = (Rockford) elementMobile;
                            this.setElement(new Empty(), x, y);
                            continue;
                        }
                        this.elementsMobile.add(elementMobile);
                        this.setElement(new Empty(), x, y);
                    } else {
                        this.setElement(element, x, y);
                    }
                }
            }
        }
    }

    /**
     * Generates the elements in the map, to position
     *
     * @param fileName Name of the file
     * @throws IOException Exception
     */
    private void loadFile(final String fileName) throws IOException
    {
        final BufferedReader buffer = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + fileName)));
        String line;
        line = buffer.readLine();
        this.width = Integer.parseInt(line);
        line = buffer.readLine();
        this.height = Integer.parseInt(line);
        line = buffer.readLine();
        while (line != null) {
            this.content += line;
            line = buffer.readLine();
        }
        buffer.close();
    }

    /**
     * Get width
     *
     * @return Width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Set width
     *
     * @param width width of the map
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * Get height
     *
     * @return Height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     *
     *
     * @param height height of the map
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Get content
     *
     * @return Content
     */
    public String getContent()
    {
        return content;
    }

    /**
     * Get element
     *
     * @return Element
     */
    public Element[][] getElements()
    {
        return elements;
    }

    /**
     * Get point x and y
     *
     * @param point Get point
     * @return x and y coordinates
     */
    public Element get(Point point)
    {
        return get(point.x, point.y);
    }

    /**
     * Get rockford position
     *
     * @param x x point
     * @param y y point
     * @return Element x, y
     */
    public Element get(final int x, final int y)
    {
        if (rockford != null && rockford.getPosition().x == x && rockford.getPosition().y == y) return rockford;
        Mobile mobile = getMobile(x, y);
        if (mobile != null) return mobile;
        return getElement(x, y);
    }

    /**
     * Get element
     *
     * @param x x point
     * @param y y point
     * @return Element with x, y point
     */
    public Element getElement(final int x, final int y)
    {
        return this.elements[x][y];
    }

    /**
     * Get element
     *
     * @param point x, y coordinates
     * @return Element with x and y position
     */
    public Element getElement(final Point point)
    {
        return this.elements[point.x][point.y];
    }

    /**
     * Add mobile
     *
     * @param mobile Add mobile element
     */
    public void addMobile(Mobile mobile)
    {
        elementsMobile.add(mobile);
    }

    /**
     * Get mobile
     *
     * @param x x point
     * @param y y point
     * @return Mobile element
     */
    public Mobile getMobile(final int x, final int y)
    {
        for (Mobile mobile : elementsMobile) {
            if (mobile.getPosition().x == x && mobile.getPosition().y == y) {
                return mobile;
            }
        }
        return null;
    }

    /**
     * Get mobile
     *
     * @param point x, y coordinates
     * @return mobile element with x and y position
     */
    public Mobile getMobile(final Point point)
    {
        return getMobile(point.x, point.y);
    }

    /**
     * Set element
     *
     * @param element Element
     * @param x x point
     * @param y y point
     */
    public void setElement(final Element element, final int x, final int y)
    {
        this.elements[x][y] = element;
        update();
    }

    /**
     * Set element
     *
     * @param element Element
     * @param point x, y coordinates
     */
    public void setElement(final Element element, final Point point)
    {
        setElement(element, point.x, point.y);
    }

    /**
     * Update map
     */
    public void update()
    {
        setChanged();
        notifyObservers();
    }

    /**
     * Get Rockford
     *
     * @return Rockford
     */
    public Rockford getRockford()
    {
        return rockford;
    }

    /**
     * Set Rockford
     *
     * @param rockford The player
     */
    public void setRockford(Rockford rockford)
    {
        this.rockford = rockford;
    }

    /**
     * Get door
     *
     * @return Door
     */
    public Door getDoor()
    {
        return door;
    }
}
