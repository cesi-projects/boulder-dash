package fr.exia.boulderdash.model;

import fr.exia.boulderdash.contract.Direction;
import fr.exia.boulderdash.contract.Sounds;
import fr.exia.boulderdash.model.dao.LevelDAO;
import fr.exia.boulderdash.model.db.DBConnection;
import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.block.Empty;
import fr.exia.boulderdash.model.element.mobile.Diamond;
import fr.exia.boulderdash.model.element.mobile.Mob;
import fr.exia.boulderdash.model.element.mobile.Mobile;
import fr.exia.boulderdash.model.element.mobile.Rockford;
import fr.exia.boulderdash.view.SoundManager;

import java.util.ArrayList;

/**
 * The class Level
 */
public class Level {
    private User user;
    private int id;
    private int lvlNumber;
    private String name;
    private Map map;
    private boolean isPaused = false;
    private boolean isFinished = false;
    private boolean doorIsOpened = false;
    private int diamondsCollected = 0;

    public Level() {
    }

    /**
     * The level characteristics
     *
     * @param id        Id of the level
     * @param lvlNumber Number of the level
     * @param map       The map
     * @param name      The name of level
     */
    public Level(int id, int lvlNumber, String name, Map map) {
        this.id = id;
        this.lvlNumber = lvlNumber;
        this.name = name;
        this.map = map;
    }

    /**
     * Get first level
     *
     * @return level 1
     */
    public static Level getFirst() {
        LevelDAO levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());
        Level firstLevel = levelDAO.findFirstLevel();
        if (firstLevel == null) {
            try {
                throw new Exception("level not found");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return firstLevel;
    }

    /**
     * Get all level
     *
     * @return All level
     */
    public static Level[] getAll() {
        LevelDAO levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());
        ArrayList<Level> allLV = levelDAO.getAll();
        return allLV.toArray(new Level[0]);
    }

    /**
     * Get level number
     *
     * @return Level number
     */
    public int getLvlNumber() {
        return lvlNumber;
    }

    /**
     * Set level number
     *
     * @param lvlNumber Number of the level
     */
    public void setLvlNumber(int lvlNumber) {
        this.lvlNumber = lvlNumber;
    }

    /**
     * Get map
     *
     * @return Map
     */
    public Map getMap() {
        return map;
    }

    /**
     * @param map Set the map
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * Get id
     *
     * @return The id of the map
     */
    public int getId() {
        return id;
    }

    /**
     * Get name
     *
     * @return The name of the map
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     *
     * @param name The name of the map
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The door is open
     */
    private void openDoor() {
        getMap().setElement(getMap().getDoor(), getMap().getDoor().getPosition());
        this.doorIsOpened = true;
    }

    /**
     * Get Diamonds remaining
     *
     * @return the number of diamond remaining
     */
    public int getDiamondsRemaning() {
        return getMap().diamonds.size();
    }

    /**
     * Get diamonds collected
     *
     * @return Number of diamond collect
     */
    public int getDiamondsCollected() {
        return diamondsCollected;
    }

    /**
     * The number of collected diamond
     *
     * @param diamond Collected diamond
     */
    public void collectDiamond(Diamond diamond) {
        getMap().diamonds.remove(diamond);
        getMap().getElementsMobile().remove(diamond);
        diamondsCollected++;
        SoundManager.play(Sounds.COLLECT_DIAMOND); // TODO At first the sound is not loaded
        if (getMap().diamonds.isEmpty())
            openDoor();
    }

    /**
     * Game loop. Refresh the game elements and animate the sprites.
     */
    public void start() {
        try {
            isPaused = false;
            isFinished = false;
            while (!isFinished()) {
                if (!isPaused()) {
                    checkWin();
                    manageFall();
                    animateMobs();
                }
                Thread.sleep(175);
                getMap().update();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void manageFall() {
        int damage = 0;
        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {
                Element element = map.get(x, y);
                if (!(element instanceof Mobile) || !element.hasGravity()) {
                    continue;
                }
                Mobile mobile = (Mobile) element;

                // Below
                if (map.get(x, y + 1) instanceof Empty) {
                    mobile.move(Direction.DOWN);
                    damage++;
                    map.update();
                    continue;
                }

                // Bellow right
                if (map.get(x + 1, y + 1) instanceof Empty && map.get(x + 1, y) instanceof Empty) {
                    mobile.move(Direction.RIGHT);
                    map.update();
                    continue;
                }

                // Bellow left
                if (map.get(x - 1, y + 1) instanceof Empty && map.get(x - 1, y) instanceof Empty) {
                    mobile.move(Direction.LEFT);
                    map.update();
                }
                if (map.get(x, y + 1) instanceof Rockford && damage >= 2) {
                    die();

                } else if (map.get(x, y + 1) instanceof Rockford || map.get(x, y + 1).isBlocking() && damage <= 2) {
                    damage = 0;
                }
            }
        }
    }

    /**
     * Check if user have win, if win the door is open.
     */
    private void checkWin() {
        if (getMap().diamonds.isEmpty() && !isDoorIsOpened())
            openDoor();
    }

    /**
     * The level is finish and the player go to the next level
     */
    public void finish() {
        isFinished = true;
        isPaused = false;
        getMap().setRockford(null);
        SoundManager.play(Sounds.FINISH_WIN);
        user.levelUp();
    }

    /**
     * Insert a new level
     *
     * @return New level
     */
    public boolean insert() {
        LevelDAO levelDAO = new LevelDAO(DBConnection.getInstance().getConnection());
        return levelDAO.create(this);
    }

    /**
     * The level has been completed
     *
     * @return Level is finished
     */
    public boolean isFinished() {
        return isFinished;
    }

    /**
     * The level has been paused
     *
     * @return Level is paused
     */
    public boolean isPaused() {
        return isPaused;
    }

    /**
     * Set the pause
     *
     * @param isPaused The level is paused
     */
    public void setPaused(boolean isPaused) {
        this.isPaused = isPaused;
    }

    /**
     * Get Rockford
     *
     * @return Rockford in the map
     */
    public Rockford getRockford() {
        return getMap().getRockford();
    }

    /**
     * Set user
     *
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * The door is open
     *
     * @return Door open
     */
    public boolean isDoorIsOpened() {
        return doorIsOpened;
    }

    /**
     * Player die by a mob, boulder or diamond
     */
    public void die() {
        isFinished = true;
        getMap().setRockford(null);
        SoundManager.play(Sounds.FINISH_LOOSE);
    }

    /**
     * Movement of the mobs
     */
    public void animateMobs() {
        for (Mobile mobile : map.getElementsMobile()) {
            if (!(mobile instanceof Mob)) {
                continue;
            }

            int n = (int) (Math.random() * 4);

            if (n == 0) {
                Element next = map.get(mobile.getX(), mobile.getY() + 1);
                if (next instanceof Empty)
                    mobile.move(Direction.DOWN);
                if (next instanceof Rockford) {
                    mobile.move(Direction.DOWN);
                    die();
                }
                map.update();
                continue;
            }
            if (n == 1) {
                Element next = map.get(mobile.getX(), mobile.getY() - 1);
                if (next instanceof Empty)
                    mobile.move(Direction.UP);
                if (next instanceof Rockford) {
                    mobile.move(Direction.UP);
                    die();
                }
                map.update();
                continue;
            }
            if (n == 2) {
                Element next = map.get(mobile.getX() + 1, mobile.getY());
                if (next instanceof Empty)
                    mobile.move(Direction.RIGHT);
                if (next instanceof Rockford) {
                    mobile.move(Direction.RIGHT);
                    die();
                }
                map.update();
                continue;
            }
            if (n == 3) {
                Element element = map.get(mobile.getX() - 1, mobile.getY());
                if (element instanceof Empty)
                    mobile.move(Direction.LEFT);
                if (element instanceof Rockford) {
                    mobile.move(Direction.LEFT);
                    die();
                }
                map.update();
            }
        }
    }
}
