package fr.exia.boulderdash.view;

import fr.exia.boulderdash.contract.IController;
import fr.exia.boulderdash.contract.IModel;
import fr.exia.boulderdash.view.panel.MainPanel;
import fr.exia.boulderdash.view.panel.Panel;

import javax.swing.*;
import java.awt.event.KeyListener;
import java.util.Objects;

/**
 * The class ViewFrame
 */
public class ViewFrame extends JFrame
{
    private IController controller;
    private IModel model;

    /**
     *ViewFrame
     *
     * @param model The size of the map and the element in the map
     */
    public ViewFrame(IModel model)
    {
        this.model = model;

        this.setTitle("Boulder Dash");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        int height = 20 * 30 + 5;
        int width = 32 * 30 + 5;
        this.setSize(width, height + 25);

        this.setLocationRelativeTo(null);
        this.setResizable(false);
        ImageIcon icon = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("img/element/mobile/diamond.png")));
        this.setIconImage(icon.getImage());
        setPanel(new MainPanel(this));
    }

    private void removeAllKeyListeners()
    {
        for (KeyListener key : this.getKeyListeners()) {
            this.removeKeyListener(key);
        }
    }

    /**
     *setPanel
     *
     * @param panel Set the panel
     */
    public void setPanel(Panel panel)
    {
        removeAllKeyListeners();
        this.setContentPane(panel);
        this.addKeyListener(panel);
        this.setFocusable(true);
        this.revalidate();
    }

    /**
     *setController
     *
     * @param controller  Set the controller
     */
    public void setController(IController controller)
    {
        this.controller = controller;
    }

    /**
     *getController
     *
     * @return  Get controller
     */
    public IController getController()
    {
        return controller;
    }

    /**
     *getModel
     *
     * @return Get model
     */
    public IModel getModel()
    {
        return model;
    }

    /**
     *setModel
     *
     * @param model Set the model
     */
    public void setModel(IModel model)
    {
        this.model = model;
    }
}
