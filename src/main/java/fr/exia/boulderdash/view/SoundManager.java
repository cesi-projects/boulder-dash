package fr.exia.boulderdash.view;


import fr.exia.boulderdash.contract.Sounds;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

/**
 * The class SoundManger
 */
public class SoundManager
{

    private static final String DEFAULT_SOUNDS_PATH = SoundManager.class.getClassLoader().getResource("sounds/").getPath();

    /**
     *
     * @param sound The sound of the game
     */
    public static void play(final Sounds sound)
    {
        File file = new File(DEFAULT_SOUNDS_PATH + sound.getSoundName());
        try (AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file)) {
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
