package fr.exia.boulderdash.view;

import fr.exia.boulderdash.contract.IController;
import fr.exia.boulderdash.contract.IModel;
import fr.exia.boulderdash.contract.IView;

import javax.swing.*;

/**
 * The Class BoulderDashView.
 */
public class BoulderDashView implements IView, Runnable
{

    /**
     * The frame.
     */
    private ViewFrame viewFrame;

    /**
     * Instantiates a new view.
     *
     * @param model the model
     */
    public BoulderDashView(final IModel model)
    {
        this.viewFrame = new ViewFrame(model);
        SwingUtilities.invokeLater(this);
    }



    /**
     *
     * @return The view of the frame
     */
    public ViewFrame getViewFrame()
    {
        return viewFrame;
    }


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Runnable#run()
     */
    public void run()
    {
        this.viewFrame.setVisible(true);
    }

    /**
     * Sets the controller.
     *
     * @param controller the new controller
     */
    public void setController(final IController controller)
    {
        this.viewFrame.setController(controller);
    }
}
