package fr.exia.boulderdash.view.panel;

import fr.exia.boulderdash.contract.Direction;
import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.model.Map;
import fr.exia.boulderdash.model.element.Element;
import fr.exia.boulderdash.model.element.mobile.Mobile;
import fr.exia.boulderdash.model.element.mobile.Rockford;
import fr.exia.boulderdash.view.ViewFrame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * The class GamePanel
 */
public class GamePanel extends Panel implements Observer
{
    private final ViewFrame viewFrame;

    /**
     * The menu to start the game
     *
     * @param viewFrame Frame of the game
     */
    GamePanel(final ViewFrame viewFrame)
    {
        this.viewFrame = viewFrame;
        this.viewFrame.getModel().getUser().getLevel().getMap().addObserver(this);
        new Thread(viewFrame.getController()::startGame).start();
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                this.viewFrame.getController().orderPerform(Direction.LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                this.viewFrame.getController().orderPerform(Direction.RIGHT);
                break;
            case KeyEvent.VK_UP:
                this.viewFrame.getController().orderPerform(Direction.UP);
                break;
            case KeyEvent.VK_DOWN:
                this.viewFrame.getController().orderPerform(Direction.DOWN);
                break;
            case KeyEvent.VK_P:
                this.viewFrame.getController().togglePause();
                break;

        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
    }

    /**
     * The position sprite and map in the game panel
     *
     * @param graphicsContext Height, width, sprite of an element, map
     */
    @Override
    public void paintComponent(final Graphics graphicsContext)
    {
        assert graphicsContext != null;
        int defaultSpriteSize = 30;

        Level level = this.viewFrame.getModel().getUser().getLevel();
        Map map = level.getMap();

        for (int y = 0; y < map.getHeight(); y++) {
            for (int x = 0; x < map.getWidth(); x++) {
                Element element = map.getElement(x, y);
                if (element != null)
                    graphicsContext.drawImage(element.getImage(),
                                              x * defaultSpriteSize,
                                              y * defaultSpriteSize,
                                              defaultSpriteSize,
                                              defaultSpriteSize,
                                              this);
                Mobile mobile = map.getMobile(x, y);
                if (mobile == null) continue;
                graphicsContext.drawImage(mobile.getImage(),
                                          mobile.getX() * defaultSpriteSize,
                                          mobile.getY() * defaultSpriteSize,
                                          defaultSpriteSize,
                                          defaultSpriteSize,
                                          this);
            }
        }

        Rockford rockford = level.getRockford();
        if (rockford != null)
            graphicsContext.drawImage(rockford.getImage(), rockford.getX() * defaultSpriteSize, rockford.getY() * defaultSpriteSize, defaultSpriteSize, defaultSpriteSize, this);

        if (level.isPaused()) {
            try {
                Image image = ImageIO.read(this.getClass().getResourceAsStream("/img/pause_icon.png"));
                Graphics2D g2d = (Graphics2D) graphicsContext;
                int x = (this.getWidth() - image.getWidth(null)) / 2;
                int y = (this.getHeight() - image.getHeight(null)) / 2;
                g2d.drawImage(image, x, y, this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(Observable o, Object arg)
    {
        this.repaint();
    }
}
