package fr.exia.boulderdash.view.panel;

import fr.exia.boulderdash.model.Level;
import fr.exia.boulderdash.view.ViewFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * The class LevelSelectPanel
 */
public class LevelSelectPanel extends Panel {

    private final ViewFrame viewFrame;

    /**
     * The menu for select the level
     *
     * @param viewFrame Frame of the game
     */
    public LevelSelectPanel(ViewFrame viewFrame) {
        this.viewFrame = viewFrame;
        for (Level level : Level.getAll()) {

            JButton button = new JButton("Start Level " + level.getLvlNumber());
            if (viewFrame.getModel().getUser().getLevel().getLvlNumber() < level.getLvlNumber() ){
                button.setEnabled(false);
            }
            button.setAlignmentX(Component.CENTER_ALIGNMENT);
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            this.add(button, BorderLayout.CENTER);
            button.addActionListener(e -> {
                this.viewFrame.getModel().getUser().setLevel(level);
                this.viewFrame.setPanel(new GamePanel(this.viewFrame));
            });
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
