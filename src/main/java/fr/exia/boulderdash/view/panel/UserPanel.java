package fr.exia.boulderdash.view.panel;

import fr.exia.boulderdash.model.User;
import fr.exia.boulderdash.view.ViewFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * The class UserPanel
 */
public class UserPanel extends Panel
{
    private JLabel jLabel;
    private JTextField txtFieldUsr;
    private JButton OK;

    private final ViewFrame viewFrame;

    /**
     * The size of the game panel
     *
     * @param viewFrame Frame of the game
     */
    UserPanel(final ViewFrame viewFrame)
    {
        this.viewFrame = viewFrame;
        this.setSize(400, 325);

        jLabel = new JLabel();
        jLabel.setText("UserName");
        txtFieldUsr = new JTextField(20);

        this.add(jLabel, BorderLayout.SOUTH);
        this.add(txtFieldUsr, BorderLayout.NORTH);


        txtFieldUsr.requestFocus();

        OK = new JButton("OK");
        this.add(OK, BorderLayout.AFTER_LAST_LINE);
        this.viewFrame.getRootPane().setDefaultButton(OK);

        OK.addActionListener(e -> {
            if (txtFieldUsr.getText().trim().isEmpty())
                return;
            User user = User.findByUsername(txtFieldUsr.getText());
            this.viewFrame.getModel().setUser(user);
            LevelSelectPanel levelSelectPanel = new LevelSelectPanel(this.viewFrame);
            this.viewFrame.setPanel(levelSelectPanel);
        });
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
    }
}