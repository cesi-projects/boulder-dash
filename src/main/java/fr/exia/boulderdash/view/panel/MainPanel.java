package fr.exia.boulderdash.view.panel;

import fr.exia.boulderdash.view.ViewFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;

/**
 * The class MainPanel
 */
public class MainPanel extends Panel
{
    private final ViewFrame view;

    /**
     * The logo of the game appears when you launch the game
     *
     * @param view The panel
     */
    public MainPanel(final ViewFrame view)
    {
        this.view = view;
        URL resource = this.getClass().getClassLoader().getResource("img/boulderlogo.png");
        assert resource != null;
        JLabel label = new JLabel(new ImageIcon(resource));
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.WHITE);
        this.add(label);
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_ENTER)
            view.setPanel(new UserPanel(view));
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
    }
}
