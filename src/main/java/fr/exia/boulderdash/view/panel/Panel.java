package fr.exia.boulderdash.view.panel;

import javax.swing.*;
import java.awt.event.KeyListener;

/**
 * The abstract class Panel
 */
public abstract class Panel extends JPanel implements KeyListener
{
}
