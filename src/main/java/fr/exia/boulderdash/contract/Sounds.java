package fr.exia.boulderdash.contract;


public enum Sounds
{
    FINISH_WIN("finish_win.wav"),
    FINISH_LOOSE("finish_loose.wav"),
    COLLECT_DIAMOND("collect_diamond.wav");

    private String soundName;

    /**
     *
     * @param soundName The sound of the element
     */
    Sounds(final String soundName)
    {
        this.soundName = soundName;
    }

    /**
     *
     * @return The sound of the element
     */
    public String getSoundName()
    {
        return soundName;
    }
}