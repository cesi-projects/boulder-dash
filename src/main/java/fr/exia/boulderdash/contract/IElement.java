package fr.exia.boulderdash.contract;

import java.awt.*;

/**
 * The Interface IElement
 */
public interface IElement
{
    /**
     * Push an element
     *
     * @return Element move
     */
    boolean isMovable();

    /**
     * Animate an element
     *
     * @return Element animated
     */
    boolean isAnimated();

    /**
     * Break an element
     *
     * @return Element break
     */
    boolean isBreakable();

    /**
     * Block an element
     *
     * @return Element block
     */
    boolean isBlocking();

    /**
     * Gravity of an element
     *
     * @return Element gravity
     */
    boolean hasGravity();

    /**
     * Get image for an element
     *
     * @return Element image
     */
    Image getImage();
}
