package fr.exia.boulderdash.contract;

import fr.exia.boulderdash.model.User;
import fr.exia.boulderdash.model.element.mobile.Rockford;

/**
 * The Interface IModel
 */
public interface IModel
{
    /**
     * Set user
     *
     * @param user The user
     */
    void setUser(User user);

    /**
     * Get user
     *
     * @return User
     */
    User getUser();

    /**
     * Set Rockford
     *
     * @param rockford Element player
     */
    void setRockford(Rockford rockford);

    /**
     * Get Rockford
     *
     * @return Rockford
     */
    Rockford getRockford();
}
