package fr.exia.boulderdash.contract;

import fr.exia.boulderdash.view.ViewFrame;

/**
 * The Interface IView.
 */
public interface IView
{
    /**
     * setController
     *
     * @param controller the controller is set
     */
    void setController(IController controller);

    /**
     *
     * @return View Frame
     */
    ViewFrame getViewFrame();
}
