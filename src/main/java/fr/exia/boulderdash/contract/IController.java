package fr.exia.boulderdash.contract;

import fr.exia.boulderdash.model.User;

/**
 * The Interface IController
 */
public interface IController
{
    /**
     * Order perform.
     *
     * @param direction the controller order
     */
    void orderPerform(Direction direction);

    /**
     * Toggle pause
     */
    void togglePause();

    /**
     * Start the game
     */
    void startGame();
}
