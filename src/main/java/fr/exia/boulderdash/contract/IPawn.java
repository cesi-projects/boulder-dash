package fr.exia.boulderdash.contract;

import java.awt.*;

/**
 * The Interface IPawn
 */
public interface IPawn
{
    /**
     * Gets the x position.
     *
     * @return the x
     */
    int getX();

    /**
     * Gets the y position.
     *
     * @return the y
     */
    int getY();

    /**
     * Gets the position Point(int x, int y).
     *
     * @return the position
     * @see Point
     */
    Point getPosition();

    /**
     * setPosition
     *
     * @param position Set the position
     */
    void setPosition(Point position);
}
