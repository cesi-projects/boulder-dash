package fr.exia.boulderdash.contract;

public enum Direction
{
    UP,
    DOWN,
    RIGHT,
    LEFT
}
